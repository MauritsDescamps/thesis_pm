import pandas as pd
from datacq.stations import STATIONS
import urllib.request
import re
import os.path
import _pickle as pickle

YEAR = 2020

baseURL = "https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?CountryCode=BE&CityName=&Pollutant=5&Year_from=2013&Year_to=2020&Station=&Samplingpoint=&Source=E1a&Output=TEXT&UpdateDate=&TimeCoverage=Year"


def getDataFrame(startYear, endYear, stationCode):
    li = []
    if (not os.path.isfile("nbCodeDict.txt")):
        retrieveNbCodeDict()
    with open("nbCodeDict.txt", 'rb') as f:
        nbCodeDict = pickle.load(f)
    stationId = str(nbCodeDict[stationCode])
    with open("urls.txt", mode='r', encoding='utf-8-sig') as file:
        urls = file.readlines()
    for year in range(startYear,endYear+1):
        try:
            url = next(url for url in urls if (str(year) in url) and (stationId in url))
        except StopIteration as e:
            print(f"No csv for year {year}: "+ str(e))
        df = pd.read_csv(url, index_col=None, header=0)
        df.drop(['Countrycode', 'Namespace', 'AirQualityNetwork',
            'AirQualityStation', 'SamplingPoint',
            'SamplingProcess', 'Sample',
            'AirPollutantCode', 'AirPollutant'],
            axis=1, inplace=True)
        li.append(df)
    return pd.concat(li, axis=0, ignore_index=True)

def retrieveNbCodeDict():
    if (not os.path.isfile("urls.txt")):
        urllib.request.urlretrieve(baseURL, "urls.txt")
    with open("urls.txt", mode='r', encoding='utf-8-sig') as file:
        urls = file.readlines()
        urls = [url.strip() for url in urls if str(YEAR) in url]
    locations= urls

    nbCodeDict = {}
    for i,url in enumerate(urls):
        data = pd.read_csv(url, nrows=10)
        stationCode = data.at[0,'AirQualityStationEoICode']
        try:
            locations[i] = STATIONS[stationCode]["long_name"]
        except KeyError:
            locations[i] = ""
        nbCodeDict[stationCode] = re.search(r'_([\d]*?)_'+str(YEAR), url).group(1)
        print(url,locations[i])
    with open('nbCodeDict.txt', 'wb') as f:
        pickle.dump(nbCodeDict, f, -1)