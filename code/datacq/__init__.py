import pandas as pd
DATADIR = "data"
STATIONS_METADF = pd.read_csv(f"{DATADIR}/stations_meta.csv", index_col=0)
import datacq.eea
import datacq.era5
import numpy as np



def get_df(years, stationCode, era5_params, pollutants, verbose=False):
    # Download/load data
    print("Retrieving pollution data...")
    df_pollutants  = datacq.eea.get_df(years, stationCode, pollutants, verbose=verbose)
    print("Retrieving ERA5 data...")
    df_era5, variable_map = datacq.era5.get_df(years, stationCode, era5_params, verbose=verbose)

    # Fix time zone
    if df_era5.index.tzinfo:
        df_era5 = df_era5.tz_convert(df_pollutants.index[0].tz)
    else:
        df_era5 = df_era5.tz_localize(df_pollutants.index[0].tz)

    # Merge dataframes
    df_full = pd.concat([df_pollutants, df_era5], axis=1)

    return df_full, variable_map