from os import dup
import pandas as pd
from datacq import DATADIR
from datacq import STATIONS_METADF
import urllib.request
import re
import os.path
import json

# Check https://discomap.eea.europa.eu/map/fme/AirQualityExport.htm

BASE_URL = "https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?"
PM10_URL = "https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?CountryCode=BE&CityName=&Pollutant=5&Year_from=2013&Year_to=2020&Station=&Samplingpoint=&Source=E1a&Output=TEXT&UpdateDate=&TimeCoverage=Year"
NOX_URL = "https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?CountryCode=BE&CityName=&Pollutant=9&Year_from=2013&Year_to=2020&Station=&Samplingpoint=&Source=E1a&Output=TEXT&UpdateDate=&TimeCoverage=Year"
SO2_URL = "https://fme.discomap.eea.europa.eu/fmedatastreaming/AirQualityDownload/AQData_Extract.fmw?CountryCode=BE&CityName=&Pollutant=1&Year_from=2013&Year_to=2020&Station=&Samplingpoint=&Source=E1a&Output=TEXT&UpdateDate=&TimeCoverage=Year"



def get_df(years, stationCode, pollutants, verbose=False):
    dicts = retrieveNbCodeDict(pollutants)
    df_list = []

    for pol in pollutants:
        if verbose: print(f"Retrieving {pol} data")
        if stationCode not in dicts[pol].keys():
            if verbose: print(f"No {pol} data for station {stationCode}")
            continue
        stationId = str(dicts[pol][stationCode])
        df_list_pol = []
        for year in years:
            data_dir = os.path.join(DATADIR,"stations",str(stationCode), str(pol), str(year), "")
            if not os.path.exists(data_dir):
                os.makedirs(data_dir)
            # Check if data is already downloaded
            if not os.path.isfile(os.path.join(data_dir,"data.csv")):
                urls = getURLS(pol, year)
                try:
                    # Find url with the correct year and stationId
                    url = next(url for url in urls if (str(year) in url) and (stationId in url))
                    temporary_df  = processEeaUrl(url,year,data_dir)
                    df_list_pol.append(temporary_df)
                except StopIteration as e:
                    if verbose: print(f"No {pol} data for year {year} and station {stationId}: "+ str(e))
            else:
                temporary_df = pd.read_csv(os.path.join(data_dir,"data.csv"), index_col=None, header=0)
                df_list_pol.append(temporary_df)
        if len(df_list_pol)>0:
            df_pol = pd.concat(df_list_pol, axis=0, ignore_index=True)
            # Sanity checks
            assert len(df_pol["AveragingTime"].unique())==1, "Inconsistent Averaging Times: "+", ".join(df_pol["AveragingTime"].unique())
            assert df_pol.at[0,"AirQualityStationEoICode"]==stationCode, "Incorrect EoICode"
            if len(df_pol["UnitOfMeasurement"].unique())!=1:
                if (set(df_pol["UnitOfMeasurement"])=={"['mg/m3']", "['µg/m3']"} or 
                    set(df_pol["UnitOfMeasurement"])=={'mg/m3', 'µg/m3'}):
                    df_pol.loc[df_pol["UnitOfMeasurement"].str.contains('mg/m3'), "Concentration"] *= 1000
                else:
                    raise ValueError(f"Inconsistent units: {df_pol['UnitOfMeasurement'].unique()}")
            df_pol = df_pol[(df_pol['Validity']==1) & (df_pol['Verification']==1)]
            df_pol.drop(['AirQualityStationEoICode','AveragingTime','UnitOfMeasurement', 'Validity', 'Verification'], axis=1, inplace=True) 
            df_pol['DatetimeBegin'] = pd.to_datetime(df_pol['DatetimeBegin'])
            df_pol.set_index(['DatetimeBegin'], inplace=True)
            df_pol.rename(columns={'Concentration':pol}, inplace=True)
            df_pol = df_pol.groupby(df_pol.index).mean() #Deal with multiple samples
            df_list.append(df_pol)
        else:
            print(f"No {pol} data for station {stationCode}")

    if len(df_list)!=0:
        df = pd.concat(df_list, axis=1)
    else:
        df = pd.DataFrame()
    return df

def processEeaUrl(url, year, data_dir):
    df = pd.read_csv(url, index_col=None, header=0)
    info_dict = {}
    info_cols = ['Countrycode', 'Namespace',
                'AirQualityNetwork', 'AirQualityStation',
                'SamplingPoint', 'SamplingProcess',
                'Sample', 'AirPollutantCode',
                'AirPollutant']
    for col in info_cols:
        unique_values = df[col].dropna().unique()
        if len(unique_values)>1:
            print(f"\tInconsistent values in {col}-column for {year}: {unique_values}")    
        elif len(unique_values)==0:
            print(f"\tEmpty {col}-column for {year}: {unique_values}") 
        info_dict[col] = unique_values[0] if len(unique_values)>0 else "nan"

    df.drop(info_cols, axis=1, inplace=True)
    df.drop('DatetimeEnd', axis=1, inplace=True)
    with open(os.path.join(data_dir,'info'), 'w') as file:
        json.dump(info_dict,file, indent=4)
    df.to_csv(os.path.join(data_dir,"data.csv"), index=False)
    return df


def getURLS(pollutant, year):
    #TimeCoverage=Year"
    url = BASE_URL\
            +"CountryCode=BE&"\
            +"Pollutant="+pollutant+"&"\
            +"Year_from="+str(year)+"&"\
            +"Year_to="+str(year)+"&"\
            +"Station=&"\
            +"Samplingpoint=&"\
            +"Source=E1a&"\
            +"Output=TEXT&"\
            +"UpdateDate=&"\
            +"TimeCoverage=Year"
    urls = urllib.request.urlopen(url).read().decode('utf-8-sig').splitlines()
    urls = [url.strip() for url in urls]
    return urls

def retrieveNbCodeDict(pollutants, year=2020):
    year = 2020 #year used for finding station ids
    masterDict = {}
    for pol in pollutants:
        try: 
            f = open(os.path.join(DATADIR,"pol_dicts", f"nbCodeDict_{pol}.json"), 'r')
            nbCodeDict = json.load(f)
        except OSError:
            urls  = getURLS(pol,year)
            nbCodeDict = {}
            for url in urls:
                data = pd.read_csv(url, nrows=10)
                stationCode = data.at[0,'AirQualityStationEoICode']
                print(url,stationCode)
                nbCodeDict[stationCode] = re.search(r'_([\d]*?)_'+str(year), url).group(1)
            with open(os.path.join(DATADIR,"pol_dicts", f'nbCodeDict_{pol}.json'), 'w') as f:
                json.dump(nbCodeDict,f, indent=4)
        masterDict[pol] = nbCodeDict
    return masterDict

def findCommonStations(dict1,dict2):
    commonKeys = dict1.keys() & dict2.keys()
    intersection = {key: dict1[key] for key in commonKeys}
    return intersection

def process_metadata(metadata_file, save=None):
    if not save:
        save = os.path.join(DATADIR, "stations_meta.csv")
    df_all = pd.read_csv(metadata_file)
    df_all = pd.DataFrame(df_all[df_all.columns[0]].apply(lambda x: x.split('\t')).to_list(), columns=df_all.columns[0].split('\t'))
    df_select = df_all[ (df_all.Countrycode=='BE') &
                        (df_all.EquivalenceDemonstrated=='yes') &
                        (df_all['AirPollutantCode']==
                            'http://dd.eionet.europa.eu/vocabulary/aq/pollutant/5')
                    ].copy()
    df_select.InletHeight = df_select.InletHeight.astype(int)
    df_select = (df_select.groupby('AirQualityStationEoICode', group_keys=False)
                    .apply(lambda x: x.loc[x.InletHeight.idxmax()]))
    df_select.drop(['Countrycode', 'Timezone','Projection', 'AirPollutantCode', 'AirQualityStation'], axis=1, inplace=True)
    df_select.reset_index(drop=True, inplace=True)
    df_select.set_index('AirQualityStationEoICode', inplace=True)
    df_select.index.name = 'stationCode'
    file = 'data/stations_meta.csv'
    df_select.to_csv(file, index=True)
    return df_select

    