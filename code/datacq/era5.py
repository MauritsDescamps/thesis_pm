import os.path
from datacq import DATADIR
from datacq import STATIONS_METADF
import math
import cdsapi
import xarray as xr
import pandas as pd
import cfgrib
MAXITEMS = 120_000

#https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=form

def get_params():
    with open('data/era5_params.txt', 'r') as f:
        era5_params = [p.strip() for p in f.readlines()]
        
    return era5_params

def get_df(years, stationCode, variables, src='reanalysis-era5-land', verbose=False):
    # Land src: 'reanalysis-era5-land' 
    # Single levels: 'reanalysis-era5-single-levels'
    c = cdsapi.Client()
    area = getArea(stationCode)
    df_list=[]
    grib_path = os.path.join(DATADIR,"temp.grib")
    variable_map = {}

    var_sets = []
    for var in variables:
        var_names_short = set()
        shortname = None
        if verbose: print(f"Retrieving {var} data from {stationCode}")
        df_list_var = []
        varDir = os.path.join(DATADIR,"stations", stationCode,var)
        if not os.path.exists(varDir):
            os.makedirs(varDir)
        downloadedYears = [int(year) for year in os.listdir(varDir) 
                            if os.path.isfile(os.path.join(varDir,year,"data.csv"))]

        missingYears = [year for year in years if year not in downloadedYears]
        if missingYears:
            src='reanalysis-era5-land'
            if verbose: print(f'\tMissing years: {missingYears}')
            else: print(f'Downloading {var} data for {missingYears}')
            req = getRequestSkeleton()
            req['area'] = area
            req['year'] = missingYears
            req['variable'] = [var]
            try:
                c.retrieve(src, req, grib_path)
            except Exception as e:
                if str(e)=="no data is available within your requested subset. Request returned no data.":
                    print(f"No {var} data available in {src}")
                    src = 'reanalysis-era5-single-levels'
                    print(f"Retrying with {src}")
                    c.retrieve(src, req, grib_path)
                else:
                    return
            ds = cfgrib.open_dataset(grib_path, engine='cfgrib')
            temporary_df = ds.to_dataframe()
            if verbose: print(f'Columns: {temporary_df.columns}')
            temporary_df.drop(['surface','step','number'], axis=1, inplace=True, errors='ignore') #Ignore "not present in index" errors
            temporary_df.reset_index(drop=True, inplace=True)
            temporary_df = temporary_df.rename(columns={'valid_time':'DatetimeBegin'})
            temporary_df.set_index(['DatetimeBegin'], inplace=True)
            for year in missingYears:
                path = os.path.join(varDir, str(year))
                try:
                    os.makedirs(path)
                except FileExistsError:
                    pass
                try:
                    temporary_df.loc[str(year)].to_csv(os.path.join(path, "data.csv"))
                except KeyError:
                    print(f"No {var} data for {year}")
            name = temporary_df.columns[0]
            var_names_short.add(name)
            if not shortname: shortname = name
            if name!=shortname:
                temporary_df.columns = [shortname]
            df_list_var.append(temporary_df)
        for year in downloadedYears:
            path = os.path.join(varDir, str(year), "data.csv")
            temporary_df = pd.read_csv(path, index_col=None, header=0)
            temporary_df.drop(['surface','step','number', 'depthBelowLandLayer'], axis=1, inplace=True, errors='ignore')
            temporary_df['DatetimeBegin'] = pd.to_datetime(temporary_df['DatetimeBegin'])
            temporary_df.set_index(['DatetimeBegin'], inplace=True)
            name = temporary_df.columns[0]
            var_names_short.add(name)
            if not shortname: shortname = name
            if name!=shortname:
                temporary_df.columns = [shortname]
            df_list_var.append(temporary_df)
        df_var = pd.concat(df_list_var, axis=0)
        variable_map[df_var.columns.values[0]] = var
        # Make time-zone unaware
        if not df_var.index.tzinfo is None: df_var.index = df_var.index.tz_localize(None)
        if verbose and len(var_names_short)>1:
            print(f'Multiple short names for {var}: {var_names_short}')
        df_list.append(df_var)
        var_sets.append(var_names_short)
    
    df = pd.concat(df_list, axis=1)
    col_names = list(df)
    counts = [col_names.count(x) for x in col_names]
    if any(c>1 for c in counts):
        ii = [i for i,x in enumerate(counts) if x>1]
        for i in ii:
            varset = var_sets[i]
            varset.remove(col_names[i])
            if len(varset)>0:
                varset = [var for var in varset if var not in col_names]
                print(f'Using alternative name for {col_names[i]}: {varset}')
                col_names[i] = varset[0]
                break
        df.columns = col_names
    return df, variable_map



def getArea(stationCode):
    margin = 0.01
    lat = STATIONS_METADF.at[stationCode, 'Latitude']
    lon = STATIONS_METADF.at[stationCode, 'Longitude']
    lat_plus = lat+margin
    lat_min = lat-margin
    lng_plus = lon+margin
    lng_min = lon-margin
    return [lat_plus, lng_min, lat_min, lng_plus]
    


def lambert72toSpherical(x,y):
    LongRef = 0.076042943        #=4°21'24"983
    nLamb = 0.7716421928
    aCarre = 6378388**2
    bLamb = 6378388 * (1 - (1 / 297))
    eCarre = (aCarre - bLamb**2) / aCarre
    KLamb = 11565915.812935
    
    eLamb = math.sqrt(eCarre)
    
    Tan1 = (x - 150000.01256) / (5400088.4378 - y)
    Lambda = LongRef + (1 / nLamb) * (0.000142043 + math.atan(Tan1))
    RLamb = math.sqrt((x - 150000.01256)**2 + (5400088.4378 - y)**2)
    
    TanZDemi = (RLamb / KLamb)**(1 / nLamb)
    Lati1 = 2 * math.atan(TanZDemi)
    
    while True:
        eSin = eLamb * math.sin(Lati1)
        Mult1 = 1 - eSin
        Mult2 = 1 + eSin
        Mult = (Mult1 / Mult2)**(eLamb / 2)
        LatiN = (math.pi / 2) - (2 * (math.atan(TanZDemi * Mult)))
        Diff = LatiN - Lati1
        Lati1 = LatiN
        if abs(Diff) <= 0.0000000277777:
            break

    lat = (LatiN * 180) / math.pi
    lng = (Lambda * 180) / math.pi
    return lat, lng

def getRequestSkeleton():
    skeleton =  {
    'product_type': 'reanalysis',
    'format': 'grib',
    'time': [
        '00:00', '01:00', '02:00',
        '03:00', '04:00', '05:00',
        '06:00', '07:00', '08:00',
        '09:00', '10:00', '11:00',
        '12:00', '13:00', '14:00',
        '15:00', '16:00', '17:00',
        '18:00', '19:00', '20:00',
        '21:00', '22:00', '23:00',
    ],
    'month': [
        '01', '02', '03',
        '04', '05', '06',
        '07', '08', '09',
        '10', '11', '12',
    ],
    'day': [
        '01', '02', '03',
        '04', '05', '06',
        '07', '08', '09',
        '10', '11', '12',
        '13', '14', '15',
        '16', '17', '18',
        '19', '20', '21',
        '22', '23', '24',
        '25', '26', '27',
        '28', '29', '30',
        '31']}
    return skeleton

