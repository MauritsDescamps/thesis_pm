from shutil import register_unpack_format
import numpy as np
import pandas as pd
from scipy.stats import norm

def remove_time(df):
    df_index = df.index.remove_unused_levels()
    date_idx = pd.to_datetime(df_index.unique(1).date)
    new_index = df_index.set_levels(date_idx, level=1)
    df.index = new_index
    return df

def calc_aggregate(df, max_nan=4, agg_func='mean', vars=None):
    if not vars:
        df = df
    else:
        df = df[vars]
    grouper = ([df.index.get_level_values('station')]
                            +[pd.Grouper(freq='D', level='time')])
    df_agg_nancount = (df.isna().groupby(grouper).sum())
    df_agg = (df.groupby(grouper).agg(agg_func))
    df_agg[df_agg_nancount>max_nan] = np.nan
    #Remove hour data from index
    df_agg = remove_time(df_agg)
    return df_agg


def transform_windvars(df, angle=False):
    assert 'u10' in df.columns, "u10 not in columns"
    assert 'v10' in df.columns, "v10 not in columns"
    df['wsp'] = np.sqrt(df['u10']**2 + df['v10']**2)
    if angle:
        df['wdir'] = np.arctan2(df['v10'], df['u10'])
    else:
        df['wdirsin'] = df['v10']/df['wsp'] #sin(theta) = y/(x^2+y^2)
        df['wdircos'] = df['u10']/df['wsp'] #cos(theta) = x/(x^2+y^2)
    df.drop(['u10','v10'], axis=1, inplace=True)
    return df

def transform_humidity(df):
    Rdry=287.0597; Rvap=461.5250
    a1=611.21; a3=17.502
    a4=32.19; T0=273.16

    dewpoint = df['d2m'].values
    pressure = df['sp'].values
    temp = df['t2m'].values
    E=a1*np.exp(a3*(dewpoint-T0)/(dewpoint-a4))
    E_sat = a1*np.exp(a3*(temp-T0)/(temp-a4))
    h =  E/(pressure-((1-Rdry/Rvap)*E))
    h_sat = E_sat/(pressure-((1-Rdry/Rvap)*E_sat))
    df['rh'] = 100*h/h_sat
    return df


       
def sin_transformer(x, period):
	return np.sin((x / period) * 2 * np.pi)

def cos_transformer(x, period):
	return np.cos((x / period) * 2 * np.pi)

def add_dailyvars(df):
    days = df.index.get_level_values('time').hour
    df['dailysin'] = sin_transformer(days,24)
    df['dailycos'] = cos_transformer(days,24)
    return df

def add_weeklyvars(df):
    hours = df.index.get_level_values('time').day_of_week
    df['weeklysin'] = sin_transformer(hours,7)
    df['weeklycos'] = cos_transformer(hours,7)
    return df

def add_yearlyvars(df):
    doy = df.index.get_level_values('time').day_of_year
    df['yearlysin'] = sin_transformer(doy,365)
    df['yearlycos'] = cos_transformer(doy,365)
    return df

def add_workday(df):
    df['workday'] = (df.index.get_level_values('time').dayofweek<5).astype(int)
    return df

def add_DoW(df):
    df['DoW'] = df.index.get_level_values('time').dayofweek
    return df


def add_aggregates(df, agg_func='mean', max_nan=4, vars=None):
    vars_all = list(df)
    if not vars:
        vars = df.columns

    rolling = df[vars].rolling(window=24, min_periods=24-max_nan)
    if agg_func == 'mean':
        suffix = '_MA'
    elif agg_func == 'max':
        suffix = '_MAX'
    df_agg = rolling.agg(agg_func).add_suffix(suffix)


    df_with_agg = pd.concat([df, df_agg], axis=1)
    new_column_list = vars_all
    for v in vars:
        i = new_column_list.index(v)
        new_column_list.insert(i+1, v+suffix)
    return df_with_agg[new_column_list]

def get_pm_outliers(pm, lvl=4):
    pm_log = np.log(1+pm).reset_index(drop=True)
    mu, sig = norm.fit(pm_log[(~pm_log.isna()) & (pm_log>=1)])
    threshold = mu+lvl*sig
    outliers = pm_log[pm_log>threshold].index
    return outliers

def sceptic_mean(array_like, max_nan=2):
    #Returns nan if more than max_nan values are nan
    #Regular mean() returns nan only when all values are nan
    #Cf. https://stackoverflow.com/a/59572667/7952998
    res = array_like.mean()
    nnan = np.isnan(array_like).sum(axis=0)

    if isinstance(res, pd.Series):
        res[nnan>max_nan] = np.nan
    else:
        res = res if nnan<=max_nan else np.nan
    return res