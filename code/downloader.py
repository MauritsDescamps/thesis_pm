import datacq
import sys
import random


stationCode = sys.argv[1] #"BETR001" #"BETN063" #"BETN043" #"BETR501"
startYear = 2013
endYear = 2020
years = range(startYear,endYear+1)
reverse = False
shuf = False

if len(sys.argv)>=3:
    if "-rev" in sys.argv:
        reverse = True
    if "-rand" in sys.argv:
        shuf = True


target = "PM10"
pollutants = ["SO2", "NO2", "CO", "O3", "PM2.5", "PM10"]

era5_features = datacq.era5.get_params()



if reverse:
    era5_features.reverse()

if shuf:
    random.shuffle(era5_features)

df_full = datacq.get_df(years, stationCode, era5_features, pollutants)