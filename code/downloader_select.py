import datacq
import sys

params_full = datacq.era5.get_params_clean()
_, var_map = datacq.get_df(range(2013,2014), 'BETR001', params_full, ['PM10'])
stations = ['BETM802', 'BETR012', 'BETR740', 'BETR240', 'BETR701', 'BETN132', 'BETR501', 'BETWOL1']
vars = ['blh', 'mcc', 'tp']

reverse=False
if len(sys.argv)==2:
    if sys.argv[1]=="-rev":
        reverse = True

if reverse:
    stations.reverse()

vars_full = []
for v in vars:
    if v in var_map:
        vars_full.append(var_map[v])

vars_full.append('10m_u_component_of_wind')
vars_full.append('10m_v_component_of_wind')

pollutants = ['PM10']
startYear = 2013
endYear = 2020
years = range(startYear,endYear+1)
for stat in stations:
    print(stat)
    df_full = datacq.get_df(years, stat, vars_full, pollutants)