import math
import numpy as np

def smape(y_true, y_pred):
        return 100*np.nanmean(2 * np.abs(y_pred - y_true) / (np.abs(y_true) + np.abs(y_pred)))

def mape(y_true, y_pred):
    return 100*np.nanmean(np.abs((y_pred - y_true) / y_true))

def mae(y_true, y_pred):
    return np.nanmean(np.abs(y_pred - y_true))

def mase(y_true, y_pred):
    y_persistence = y_true[:-1]
    return mae(y_true,y_pred) / np.nanmean(np.abs(y_true[1:] - y_persistence))

def rmse(y_true, y_pred):
    return np.sqrt(np.nanmean((y_pred - y_true)**2))

def successIndex(y_true, y_pred, threshold):
    NOF = sum((y_true>threshold) & (y_pred>threshold))
    NOF_ = sum((y_true>threshold) & (y_pred<=threshold))
    NO_F = sum((y_true<=threshold) & (y_pred>threshold))
    NO_F_ = sum((y_true<=threshold) & (y_pred<=threshold))
    print(NOF, NOF_, NO_F, NO_F_)
    if (NOF+NOF_)==0 or NO_F_+NO_F==0:
        return float('NaN')
    return 100*(NOF/(NOF+NOF_) + NO_F_/(NO_F_+NO_F)-1)

metrics = {'smape': smape, 
            'mae': mae,
            'mase': mase,
            'rmse': rmse}

def evaluate(y_true, y_pred):
    return {name: metric(y_true, y_pred) for name, metric in metrics.items()}





