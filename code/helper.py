import numpy as np
import pandas as pd
import warnings


def nan_handler(predictor):
    #Returns a function that returns nan if any of the features are nan
    def wrapper(X):
        nan_ids = np.isnan(X).any(axis=1) # boolean array
        X_no_nan = X[~nan_ids,:] # samples without nan
        pred_no_nan = predictor(X_no_nan)
        pred = np.empty(X.shape[0])
        pred[:] = np.NaN
        pred[~nan_ids] = pred_no_nan
        return pred
    return wrapper


