\documentclass[aspectratio=43]{beamer}
\usetheme{Frankfurt}
\beamertemplatenavigationsymbolsempty
\input{../preambleMath.tex}
\usepackage[backend=biber, style=authortitle, doi=false,isbn=false,url=false,eprint=false]{biblatex}
\addbibresource{../pm.bib}
\graphicspath{{../../figures}}
\usepackage{booktabs}
\setbeamertemplate{footline}[frame number]

\title{Short-term Particulate Matter forecasting}
\author[]{Maurits Descamps\\[\baselineskip] 
	\parbox[t]{2.5cm}{\raggedleft
		Supervisor\texorpdfstring{\\[0\baselineskip]}{}
		Mentors
	}
	\hspace{0.2cm}
	\parbox[t]{4.5cm}{\raggedright
		Prof. Bart De Moor\\
		Konstantinos Theodorakos
		Dr. Mauricio Agudelo
	}
}

\AtBeginSection[]
{
	\begin{frame}
		\frametitle{Table of Contents}
		\tableofcontents[currentsection]
	\end{frame}
}

\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}

% Table of Contents
\begin{frame}{Outline}
	\hfill	{\large \parbox{.961\textwidth}{\tableofcontents[hideothersubsections]}}
\end{frame}

\section{Introduction}
\begin{frame}{Particulate matter}
	\begin{columns}
		\begin{column}{0.44\textwidth}
				\begin{itemize}
					\item Solid and liquid atmospheric particles
					\item $\diameter < \SI{10}{\micro\metre}$: PM10
					\item $\diameter < \SI{2.5}{\micro\metre}$: PM2.5
				\end{itemize}
		\end{column}
		\begin{column}{0.56\textwidth}
			\begin{figure}
				\centering
				\includegraphics[width=\linewidth]{PM.png}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Motivation}
	\begin{block}{}
		{\large "Air pollution was the 4th leading risk factor for early death worldwide in 2019, surpassed only by high blood pressure, tobacco use, and poor diet."}
		\vskip5mm
		\hspace*\fill{\small--- \cite{soga2020}}
	\end{block}
	Well studied effects of PM on:
	\begin{itemize}
		\item Asthma
		\item Lung cancer
		\item Cardiovascular disease
		\item Birth defects
		\item ...
	\end{itemize}
\end{frame}

\begin{frame}{Motivation}
		\begin{block}{Effect on mortality}
		$\SI{10}{\micro\gram\per\metre\cubed}$ increase in PM10
		$\rightarrow$ $0.8\%$ increase of non-accidental mortality \footnotemark
	\end{block}
	
	\begin{block}{EU Air quality Standards - PM10}
		\begin{itemize}
			\item Max. annual mean: $\SI{40}{\micro\gram\per\metre\cubed}$
			\item Max. daily mean: $\SI{50}{\micro\gram\per\metre\cubed}$\\
			(35 exceedances permitted per year)
		\end{itemize}
	\end{block}
	\footcitetext{PM_effectts_fr}
\end{frame}

%\begin{frame}{Goals}
%	Through the development of
%\end{frame}

\begin{frame}{Data}
	\onslide<+->{\begin{block}{1. Meteorological data: ERA5\footnotemark}
		\begin{itemize}
			\item Temperature, Precipitation, Radiation...
			\item Hourly data since 1950
			\item Gridded data ($0.25^{\circ}$x$0.25^{\circ}$ resolution)
		\end{itemize}
	\end{block}
	\footcitetext{era5}}
	\onslide<+->{\begin{block}{2. Pollution measurements: E1a and E2a data sets\footnotemark}
		\begin{itemize}
			\item PM, SO2, NO2, O2...
			\item Hourly data since 2013
			\item $\pm 70$ monitoring stations in Belgium
		\end{itemize}
	\end{block}
	\footcitetext{pollutants}}
\end{frame}

\section{PM10 in Belgium}
\begin{frame}{Snapshot 2013}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{PM10_hist_2013}
		\caption{2013 daily PM10 averages.}
	\end{figure}
\end{frame}

\begin{frame}{Snapshot 2019}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{PM10_hist_2019}
		\caption{2019 daily PM10 averages.}
	\end{figure}
\end{frame}

\begin{frame}{Daily averages trend}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{PM10_boxplot_Corroy-Le-Grand}
		\caption{Boxplots of daily averages year by year from a rural station.}
	\end{figure}
\end{frame}

%\begin{frame}{Annual average trend}
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.8\linewidth]{PM10_trend}
%		\caption{Annual averages}
%	\end{figure}
%\end{frame}


\section{Single day ahead forecasting}

\begin{frame}{Considered variables}
	\begin{block}{Terminology}
		\begin{itemize}
			\item $\langle PM10\rangle_t$: 24h average PM10 concentration on day t
			\item $\langle PM10\rangle_{t,9h}$: PM10 concentration averaged over the first 9 hours of day t
		\end{itemize}
	\end{block}
	\vspace{2mm}
	\begin{columns}[t]
		\uncover<2->{
		\begin{column}{0.5\textwidth}
			Pollutant measurements:
			\begin{itemize}
				\item PM10, PM2.5, NO$_2$, SO$_2$, CO, O$_3$
				\item Average of first 9h of Day$_0$\\
				$\rightarrow$ For practical reasons
				%\item Day$_{-1}$, Day$_{-2}$,...
			\end{itemize}
		\end{column}
		}
		\uncover<3->{
		\begin{column}{0.5\textwidth}
			Meteorological variables:
			\begin{itemize}
				\item 36 variables \footnotemark
				\item Average on Day$_1$\\
				$\rightarrow$ Use ECMWF forecast for practical implementation \footnotemark
			\end{itemize}
		\end{column}
	}
	\end{columns}
	\uncover<3->{
	\footcitetext{era5land}
	\footcitetext{PM10NN}}
\end{frame}

\begin{frame}{Variable selection}
	Possible criteria:
	\begin{itemize}
		\item Pearson's Correlation Coefficient (PCC): $r=\frac{\text{cov}(X,Y)}{\sigma_X\sigma_Y}$
		\item Spearman's rank correlation Coefficient: $r_s=\frac{\text{cov}(R(X),R(Y))}{\sigma_{R(X)}\sigma_{R(Y)}}$\\
		$\rightarrow$ PCC between the rank variables
		\item Linear-kernel SVM coefficient: c\textsubscript{SVM}
		\begin{itemize}
			\item Grid Search over hyperparameters ($C$ and $\epsilon$)
			\item 5-fold CV
			\item Weights $w$ give indication of feature importance
		\end{itemize}
	\end{itemize}
\vspace{3mm}
	$\rightarrow$ Evaluated for Rural station (Corroy-Le-Grand)
\end{frame}

\begin{frame}{Variable selection}
	Best 10 variables according to\footnote{From better to worse}:
	\begin{table}\centering
		\resizebox{\textwidth}{!}{%
			\input{../tables/scores.txt}
		}
	\end{table}
\end{frame}

\begin{frame}{Variable selection}
	Best 10 variables according to average rank\footnote{Lower rank is better}:
	\begin{table}\centering
		\input{../tables/top10rank.txt}
	\end{table}
\end{frame}

\begin{frame}{SVM regression}
	Workflow:
	\begin{enumerate}
		\item Select 10 best variables from earlier (range 2013-2017)
		\item Tune hyperparameters with 5-fold time series split\\
		Grid search over 48 combinations of:
		\begin{itemize}
			\item $\epsilon$ (hinge-loss)
			\item $\sigma$ (RBF bandwidth)
			\item $C$ (regularization)
		\end{itemize}
		\item Select parameters with lowest MSE
		\item Evaluate on 2018 data
	\end{enumerate}
\end{frame}

\section{Results}

\begin{frame}{Metrics: Definitions}
	\begin{itemize}
		\item Root Mean Suare Error (RMSE): $\sqrt{\frac{\sum_{t=1}^{n}(F_t-A_t)^2}{n}}$
		\item Symmetric mean absolute percentage error (SMAPE): $$\frac{100\%}{n}\sum_{t=1}^{n}\frac{\abs*{F_t-A_t}}{(\abs*{A_t}+\abs*{F_t})/2}$$
		\item Mean Absolute Scaled Error (MASE): $$\frac{\frac{1}{n}\sum_{t=1}^{n}\abs*{F_t-A_t}}{S}$$\\
		with $S$ the Mean Absolute Error of the persistence model on the training set\\
		$S=\frac{1}{n_\text{train}-1}\sum_{t=2}^{n_\text{train}}\abs*{A_{\text{train, }t}-A_{\text{train, }t-1}}$ 
	\end{itemize}
\end{frame}

\begin{frame}{Metrics}
	\begin{table}\centering
		\input{../tables/SVR_results.txt}
	\end{table}
	Observations:
	\begin{itemize}
		\item Full RBF SVR performs best
		\item Dropping 26 worst features $\rightarrow$ only slight performance loss
		\item Significant improvement over persistence model
	\end{itemize}
\end{frame}

\begin{frame}{Predictions}
	\begin{figure}\centering
		\includegraphics[width=\linewidth]{test_persistence_SVR_short}
	\end{figure}
\end{frame}


\begin{frame}{Residuals}
	\begin{figure}\centering
		\includegraphics[width=0.8\linewidth]{residuals_svr}
	\end{figure}
	Observations:
	\begin{itemize}
		\item Undershoot for peaks
		\item More overshoot with persistence model
	\end{itemize}
\end{frame}

\section{Outlook}
\begin{frame}
	Future work:
	\begin{itemize}
		\item Extend feature analysis to other types of stations
		\item Compare SVR performance with NN architectures
		\item Consider longer history for forecasting
		\item Investigate interpolation techniques, cf. \cite{RIO}
	\end{itemize}
\end{frame}

\section*{Appendix}
\begin{frame}{Scores}
	\begin{figure}
		\centering
		\includegraphics[width=0.75\linewidth]{metrics_by_R}
	\end{figure}
\end{frame}

\end{document}

