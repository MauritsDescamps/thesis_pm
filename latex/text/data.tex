\chapter{Data}
\label{c:data}
Two different sources of data will be used, one for pollutant measurements, and one for meteorological data. The nature of these datasets, and the process of accessing them will be discussed here.


\section{Air-quality data}
Throughout Belgium, there are a number of air-quality monitoring stations, reporting hourly concentrations of various pollutants including \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmtf$ and $\pmten$. These measurements are accessible through a download service provided by the European Environment Agency (EEA) \cite{pollutants}. The service gives access to two dataflows: E1a and E2a. The first one goes back to 2013 and is updated every September, when data from the previous year is added. This means that, at the time of writing, the most recent E1a measurements are from 2020. The second dataflow, E2a, is updated daily, and provides all measurements that are not yet included in E1a. The E2a measurements are, however, not yet validated, so this thesis will only consider E1a data. This means that the available pollutant measurements span eight years, from 2013 to 2020.

Out of the over 100 air-quality measurement stations in Belgium, only the 67 stations reporting $\pmtf$ concentrations are relevant for this thesis. Out of those 67, only 32 stations have less than 10\% missing $\pmtf$ values. Their locations are shown on a map in \cref{fig:stationsmap}. The coordinates of the stations and other meta data such as the altitude, the curb distance, the distance to the building and the inlet height of the sensor are provided through the download service in the form of a .csv file\footnote{Stations meta data: \url{https://discomap.eea.europa.eu/map/fme/metadata/PanEuropean\_metadata.csv}}. Additionally, the file also contains the type of each station, \texttt{background}, \texttt{industrial} or \texttt{traffic}, and a description of the area of each station, \texttt{suburban}, \texttt{rural} or \texttt{urban}. \Cref{tab:stat_counts} show how many of the 32 stations fall in each of those categories.

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{PM25_stations}
	\caption{Locations of all 53 stations reporting PM10 concentrations.}
	\label{fig:stationsmap}
\end{figure}

\begin{table}\centering
	\begin{tabular}{lrrrr}
		\toprule
		&\multicolumn{3}{c}{Area}&\\
		\cline{2-4}
		& rural & suburban & urban & $\sum$ \\
		Type &  &  &  &  \\
		\midrule
		background & 10 & 9 & 5 & 24 \\
		industrial & 2 & 4 & 0 & 6 \\
		traffic & 0 & 0 & 2 & 2 \\
		\midrule
		$\sum$ &12 & 13 & 7 & 32 \\
		\bottomrule
	\end{tabular}
	\caption{Counts of the different combinations of station types and areas.}
	\label{tab:stat_counts}
\end{table}


\section{Meteorological data}
For the meteorological data, two ERA5 data sets from the European Centre for Medium-Range Weather Forecasts (ECMWF) are used, specifically the \textit{ERA5 hourly data on single levels from 1959 to present} and the \textit{ERA5-Land hourly data from 1950 to present} data sets\cite{era5land,era5hour}. These data sets contain hourly values for a large number of atmospheric and land-surface quantities. By combining measurements and climate models, ERA5 provides estimates of these quantities on a $0.25^\circ\times0.25^\circ$ grid. In Belgium, this roughly amounts to a longitudinal resolution of $\SI{20}{\km}$ and a latitudinal resolution of $\SI{30}{\km}$. Since part of the goal of this thesis is the identification of relevant features, a substantive subset of the available ERA5 variables will be considered. The names and a short descriptions of the considered variables are given in \cref{tab:era5vars}. More detailed definitions and units for these variables are given in \cite{era5land,era5hour}. Since the pollutant measurements discussed in the previous section are only available from 2013 to 2020, only ERA5 data from these 8 years will be considered. Furthermore, since this thesis only deals with particulate matter forecasts at the locations of the measuring stations, only meteorological data at the grid cells containing those locations will be considered. %TODO: reference interpolation


\begin{longtable}{p{47mm}lp{70mm}}
	\toprule
	Variable& Short &Description\\
	\midrule
	10m u-component of  wind&u10& Eastward component of the wind at \SI{10}{\meter}.\\
	10m v-component of  wind& v10& Northward component of the wind at \SI{10}{\meter}.\\
	2m dewpoint temperature& d2m&Temperature to which the air, at 2 metres above the surface of the Earth, would have to be cooled for saturation to occur. Through temperature and pressure related to humidity.\\
	2m temperature& t2m&Temperature of air at 2m above the surface.\\
	\raggedright Evaporation from bare soil& evabs&The amount of evaporation from bare soil at the top of the land surface.\\
	Runoff& ro&Water from rainfall or melting snow that is not stored in the soil but runs off.\\
	Evaporation& e&The accumulated amount of water that has evaporated from the Earth's surface, including a simplified representation of transpiration (from vegetation), into vapour in the air above.\\
	Surface latent heat flux& slhf&The transfer of latent heat (resulting from water phase changes, such as evaporation or condensation) between the Earth's surface and the atmosphere through the effects of turbulent air motion.\\
	Surface net solar radiation& ssr&The amount of solar radiation (also known as shortwave radiation) that reaches a horizontal plane at the surface of the Earth (both direct and diffuse) minus the amount reflected by the Earth's surface (which is governed by the albedo).\\
	Surface net thermal radiation& str&Thermal radiation (also known as longwave or terrestrial radiation) refers to radiation emitted by the atmosphere, clouds and the surface of the Earth.\\
	Surface pressure& sp&The pressure (force per unit area) of the atmosphere at the surface.\\
	Surface sensible heat flux& sshf&The transfer of heat between the Earth's surface and the atmosphere through the effects of turbulent air motion (but excluding any heat transfer resulting from condensation or evaporation). \\
	\raggedright Surface solar radiation downwards& ssrd& The amount of solar radiation (also known as shortwave radiation) that reaches a horizontal plane at the surface of the Earth.\\
	\raggedright Surface thermal radiation downwards& strd&The amount of thermal (also known as longwave or terrestrial) radiation emitted by the atmosphere and clouds that reaches a horizontal plane at the surface of the Earth.\\
	Total precipitation& tp&The accumulated liquid and frozen water, comprising rain and snow, that falls to the Earth's surface.\\
	Boundary layer height& blh&The depth of air next to the Earth's surface which is most affected by the resistance to the transfer of momentum, heat or moisture across the surface.\\
	Low cloud cover& lcc&The proportion of a grid box covered by cloud occurring in the lower levels of the troposphere.\\
	Medium cloud cover& mcc&The proportion of a grid box covered by cloud occurring in the middle levels of the troposphere.\\
	High cloud cover& hcc&The proportion of a grid box covered by cloud occurring in the high levels of the troposphere. \\
	Total cloud cover&tcc& The proportion of a grid box covered by cloud.\\
	\bottomrule
	\caption{List of considered ERA5 variables with short descriptions.}
	\label{tab:era5vars}
\end{longtable}

Downloading the ERA5 variables can be done via an online interface\footnote{
	\textit{ERA5 hourly data on single levels from 1959 to present} interface: \url{https://doi.org/10.24381/cds.adbb2d47}
}\footnote{
	\textit{ERA5-Land hourly data from 1950 to present} interface: \url{https://doi/10.24381/cds.e2161bac}
}, or directly in Python using the Climate Data Store API \texttt{cdsapi}\footnote{Climate Data Store API python package: \url{https://pypi.org/project/cdsapi/\#description}}.  The API requests are limited to 120.000 values each, which suffices for 8 years of hourly values of one variable at one grid cell ($8\times365\times24 = 70.080$). Since each request takes around 1 hour to be handled, all 20 considered ERA5 variables can be downloaded for one location in roughly one day.


As discussed in \cref{c:litrev}, forecasted meteorological variables can be valuable features. All variables in \cref{tab:era5vars} are included in the ECMWF's Atmospheric Model high resolution 10-day forecast (Set I - HRES) \cite{ECMWFforecast}. This forecasting service is run 4 times a day, at 00/06/12/18, and contains hourly forecasts over a horizon of 90 hours.

\section{Air-quality station selection}
Due to the slow access to the meteorological data, and the large number of features that will be considered, it would be impractical to use data from all 32 stations. Therefore, a subset of 5 stations, shown in \cref{tab:stations}, is chosen. The pollutants \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmtf$ and $\pmten$ are reported by each of these stations except BETR802, for which \ch{O3} and \ch{SO2} are missing.


\begin{table}\centering
	\begin{tabular}{llll}
		\toprule
		Station code&Type&Area&Place\\
		\midrule
		BETN063&background&rural&Corroy-Le-Grand\\
		BETR222&background&suburban&Seraing\\
		BETR001&background&urban&Molenbeek\\
		BETN043&industrial&suburban&Haren\\
		BETR802&traffic&urban&Borgerhout\\
		\bottomrule	
	\end{tabular}
	\caption{The air quality stations considered  in this study.}
	\label{tab:stations}
\end{table}