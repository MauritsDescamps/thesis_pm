\chapter{Literature Review}
\label{c:litrev}

This chapter will discuss relevant background on particulate matter forecasting. First, \cref{s:background} discusses the phenomenon of PM and its health impacts. Then, \cref{s:potfeat} goes over the different types of variables that have been used as inputs to PM forecasting models. \Cref{s:LR-FS} discusses various feature selection methods. Next, \cref{s:models} discusses several types of models used for PM forecasting and ways of tuning their hyper-parameters. Finally, several evaluation metrics specific to time series forecasting are given in \cref{s:eval}.


%Note that some of the references in this chapter deal with $\pmten$ rather than $\pmtf$, but since these two variables are closely related, their findings can also be helpful in the context of $\pmtf$.

\section{Background}
\label{s:background}
Many specific air pollutants, such as for example \ch{SO2}, \ch{NO2}, \ch{CO} and ozone (\ch{O3}) have been shown to have negative health impacts, and until the early 1990s, ozone was believed to be the pollutant causing the most health problems and early mortality.  This changed when a 1993 paper by Dockery et al. \cite{Dockery1993} found that fine particulate matter was a major driver of excess mortality in the six US cities considered in the study\cite{Sonwani}. Since then, multiple studies have confirmed this, and Particulate Matter (PM) is now considered the form of air pollution causing by far the most harm worldwide \cite{GBD2019,EEAreport}. 


\subsection{Particulate Matter}
Rather than a single pollutant, particulate matter is an airborne mixture of solid particles and liquid droplets. Its components can originate from both natural sources such as pollen, sea salt and windblown dust, and anthropogenic sources such as agricultural systems, residential heating, traffic and fossil fuel combustion. A distinction is also made between primary pollutants, which have been directly released from their source, and secondary pollutants, which form through chemical reactions from other precursing pollutants such as \ch{SO2} and \ch{NO_{x}}{\cite{Sonwani}. An overview of these two categories is shown in \cref{fig:PMsrcs}.
	
\begin{figure}\centering
	\includegraphics[width=\linewidth]{PM_srcs}
	\caption{Composition of primary and secondary particulate matter. (Figure from \cite{Sonwani})}
	\label{fig:PMsrcs}
\end{figure}

In the context of human health, two types of particulate matter are of particular interest, namely $\pmten$ and $\pmtf$, which consist of particles with diameters less than $\SI{10}{\micro\metre}$ and $\SI{2.5}{\micro\metre}$ respectively. These are small enough to reach the lungs and can therefore cause adverse health effects. Especially the finer $\pmtf$ particles pose great health risks because they are more likely to be toxic, and because they are small enough to reach deep into the lungs and even into the blood stream \cite{Miller1979}. \Cref{fig:PM} shows the size distributions of several airborne particles, giving an idea of what is meant by $\pmtf$ and $\pmten$.
Numerous studies have linked long-term $\pmtf$ exposure to increased mortality \cite{Dockery1993,GBD2019,euroReport,burnett2018}, as well as to specific medical conditions such as respiratory tract diseases \cite{respiratory}, stroke \cite{stroke}, and lung cancer \cite{lungCancer}. Besides the effects of long-term exposure to particulate matter, there is also a well documented short term effect on both hospitalizations and mortality \cite{REVIHAAP,Heo2019}. For example, two studies, one evaluating 9 French cities and one evaluating the agglomeration of Warsaw, each found a significant effect of $\pmtf$ on non-accidental mortality the same day: $+0.7\% \text{ CI } 95\% \left[-0.1,1.6\right]$ and $+0.7\% \text{ CI } 95\% \left[0.1,1.2\right]$ for a $\SI{10}{\micro\gram\per\cubic\meter}$ increase respectively \cite{Pascal2014,Maciejewska2020}.
	

\begin{figure}\centering
	\includegraphics[width=0.7\linewidth]{PM}
	\caption{Size distribution of various types of airborne particles \cite{PMimg}.}
	\label{fig:PM}
\end{figure}

\subsection{Forecasting setups}
There are no strict definitions for what constitutes a short-term particulate matter forecast. In the extensive literature on the topic, the problem has been interpreted in many different ways. On the one hand, there are studies that focus on forecasting the average PM concentration for the next day \cite{Dutta2021,Hooyberghs2005,Rumaling2022}, or the next 2 \cite{Cecchetti2004} or three days \cite{Biancofiore2017}. Some variations of this setup are the prediction of the maximum PM concentration the next day \cite{McKendry2002}, and the prediction of the maximum of the 24h-moving average concentration over the next day \cite{Perez2002,Menares2021}. Several of these referenced studies use observed pollutant concentrations of the present day as inputs. However, when the goal of the forecast is to help policymakers decide when to initiate pollution reduction measures, a forecast that is only available at midnight is of little practical use. To this end, some authors choose to only include observations up to 9 A.M. \cite{Cecchetti2004,Hooyberghs2005} or 6 P.M.\cite{Perez2002} of the present day.

A second type of short-term forecasts predicts concentrations at specific hours in the future. Variants of this setup include a next hour forecast \cite{Huang2018}, a 24 hour ahead forecast \cite{Kukkonen2003,Grivas2005}, and a forecast of all individual hourly concentrations for the next 24 hours \cite{Perez1999}.







\section{Potential features}
\label{s:potfeat}
When dealing with a time series forecasting problem, the most obvious features are past, or lagged, values of the target time series. However, in the case of particulate matter, there are numerous external effects influencing its concentration. This means that forecasting accuracy can be increased significantly by including the right exogenous features. In past studies, various such features have been identified, they can be divided into the following four categories. 



\subsection{Pollutant measurements}
Besides lagged $\pmtf$ values, also other pollutants can be informative. The most commonly used exogenous pollutant are \ch{SO2} and \ch{NO_x}, which both directly contribute to the formation of secondary particulate matter \cite{Sonwani}. The latter of the two, \ch{NO_x}, denotes the family of nitrogen oxides, which includes \ch{N2O}, \ch{NO}, \ch{N2O2}, \ch{N2O3}, \ch{NO2}, \ch{N2O4}, and \ch{N2O5}. Specific members of this family, like \ch{NO2} and \ch{NO}, are often used as individual features as well \cite{Cecchetti2004,Rumaling2022,Dutta2021}. Another benefit of including these two pollutants, besides their direct contribution to the formation of particulate matter, is the information they convey pertaining to human activity. \ch{SO2} concentrations are to a great extent caused by the energy production sector, so \ch{SO2} is a good indicator for the total amount of heating emissions. \ch{NO_x} is a good indicator for the total amount of road transport emissions, since traffic is its most dominant source \cite{Cecchetti2004}. Other pollutant that have been used for particulate matter forecasting include \ch{O3} (\cite{Rumaling2022}) and \ch{CO} (\cite{Biancofiore2017,Dutta2021,Braik2020}).

Besides using lagged pollutant concentrations as inputs, it is also common to include aggregates such as for example the average and maximum concentration over a 24-hour window \cite{Perez2002}.

%\cite{Biancofiore2017} CO
%\cite{Cecchetti2004} NOx SO2
%\cite{Rumaling2022} CO NO2 O3 S02
%\cite{Dutta2021} NO2 SO2 CO

\subsection{Meteorological variables}
Since meteorological phenomena strongly influence both the formation and the movement of particulate matter \cite{Sonwani}, most PM forecasting models include a number of meteorological variables. These often include wind speed, wind direction, amount of rainfall, humidity, temperature, pressure, cloud cover and solar radiation.

A less known, but in the context of PM forecasting crucial meteorological variable, is the boundary layer height (BLH) \cite{survey,Huang2015}, which is defined as \textit{the depth of air next to the Earth's surface which is most affected by the resistance to the transfer of momentum, heat or moisture across the surface} \cite{era5}. It and can be used as a measure for the height up to which particles spread due to  turbulence \cite{Hooyberghs2005}. A higher boundary layer means that particles get spread over a larger volume, resulting in a decreased concentration.

Something that sets meteorological variables apart from pollutant measurements is the fact that high quality forecasts are available. Using such forecasted values as inputs to the PM forecasting model is a common practice\cite{Hooyberghs2005,Perez2002}. One issue with this approach is that historically forecasted time series are usually not available, so in practice the actual values are often used for training and testing. This means that the obtained performances provide an upper limit of what can be achieved with accurate meteorological forecasts.


\subsection{Temporal variables}
Particulate matter concentrations exhibit variations of different periodicity, including half-daily, daily, weekly and seasonal cycles \cite{Fu2020,Muhammad2019,Perez1999}. These variations can be attributed to natural cycles of meteorological variables, and to human activity such as traffic patterns, heating schedules and weekday-weekend differences. A forecasting model that would not take into account these cycles would periodically over- and underestimate PM concentrations.

There are two ways to incorporate knowledge about such cycles into a forecasting model. The first one is to deseasonalisation, and it works as follows. First, strong periodic patterns, which can be identified through Fourier analysis, are subtracted from the original time series. Next, a forecasting model is trained using the deseasonalized time series. Finally, during inference, the periodic patterns are summed with the models predictions \cite{Cecchetti2004}. 

Alternatively, a temporal input feature can be added for each cycle, signifying at what point in each cycle the forecast is made \cite{Hooyberghs2005,Rumaling2022,Grivas2005}. For yearly patters, for example, a naive way to do this is to include the day of the year, ranging from 0 to 365, as a feature. The problem with this approach is that it introduces a discontinuity at turn of the year. The first and last day of the year would have vastly different values despite being close together on the yearly cycle. To solve this, the day of the year can be encoded into a sine and a cosine with a period of 365 days. The reason why both are necessary is the fact that a single sinusoid has the same value at different points in one period.  The same can be done for the day of the week and the hour of the day, although the latter one only makes sense if the model makes hourly forecasts, rather than daily. 

\subsection{Geographical variables}
When a forecasting model is trained on data from more than one location, with the goal of applying it at different locations, it is necessary to include geographical input variables. This way the model can learn the difference in pollution dynamics between for example a busy city center and a rural village. Commonly used geographical variables can be split into three categories. The first category consists of traffic-related variables such as the distance to the nearest major road, the total traffic volume on the nearest major road, and the road density in a buffer around the location. A second category of variables is used to convey what the land around the location is used for. This is usually done in the form of percentages of different classes of land use such as for example agricultural, industrial and residential. In Europe, the Corine Land Cover (CLC) data set is often used for this purpose, it contains 44 different classes of land-use \cite{CORINE}. Another feature that could also be considered a land use variable is the population density around the location. The third category of variables adds topographical information about the location such as altitude, latitude, longitude and distance to the coast \cite{Mao2011}.

For some of these variables, such as the road density and the land use percentages, the size of the area around the location that is considered needs to be chosen. Common radii for these buffers range from $50$ meters to one or two kilometers. In some applications, the same variable is calculated over multiple buffer sizes \cite{RIO,Mao2011}.  These multiple buffer sizes, and the often large number of land use variables can, however, result in a large amount of often highly correlated geographical features. To remedy this, the dimensionality of the geographical feature space can be reduced through Principal Component Analysis (PCA) \cite{Jung2018}.






\section{Feature selection}
\label{s:LR-FS}
From the variables discussed in \cref{s:potfeat}, a large number of candidate features can be derived. In theory, one could use all those features as inputs to the forecasting model. In practice, however, a number of the candidate features will be irrelevant, redundant or noisy, and removing them makes the forecasting model simpler, reduces training time, and can lead to increased performance. There are several techniques for selecting features, most of which can be classified as filter, wrapper, and embedded methods. These different classes are briefly discussed below, a detailed survey is given in \cite{Chandrashekar2014}.

\subsection{Filter methods}
Filter methods score each feature individually on a statistical measure and discard all features that do not pass a certain threshold. Two common criteria are the Pearson correlation coefficient and the mutual information between the feature and the target. Although simple to implement and computationally efficient, filter methods have a number of drawbacks. They are, for example, often not good at capturing feature interactions. This means that a feature which is not very informative on its own, but is very useful in combination with another feature, will be discarded by most filter methods. A family of filter methods which doesn't suffer from this problem are the RELIEF-based algorithms based on the original RELIEF algorithm introduced in \cite{Kira1992}. Another drawback of most filter methods is that the threshold needs to be selected manually.


\subsection{Wrapper methods}
Wrapper methods try to find the feature subset which yields the highest performance using a chosen learning model. If there are $N$ candidate features, an exhaustive search over all feature subsets would require the training of $2^N$ models. Since this is usually prohibitively expensive, several techniques have been proposed that evaluate less combinations. Some examples are forward selection, backwards elimination and bidirectional elimination which is a combination of the former two.

\subsubsection{Artificial contrast: Boruta}
A special type of wrapper method often used in practice is the Boruta algorithm, which is built around the Random Forest (RF) model \cite{Kursa2010}. The method is based on the concept of artificial contrast proposed in \cite{Tuv2006,Tuv2009}. In short, the algorithm trains several random forests using the original features and artificial features, which are shuffled versions of the original ones or so-called shadow features. Only features which consistently outperform the best performing artificial features on an importance measure are kept. The most common feature importance measure used with Boruta is the Mean Decrease in Impurity in the case of classification, and Mean Decrease in RSS (Residual Sum of Squares) in the case of regression. Another common importance measure is the permutation importance. It is defined as the mean reduction in accuracy, e.g. MSE, resulting from the shuffling of a given feature. If the shuffling of a feature does not significantly decrease the accuracy, said feature is not important.

Lastly, a less common importance measure is based on Shapley values \cite{borutashap}. Shapley values are rooted in coalition Game Theory, and provide a way of fairly distributing a pot over a number of players \cite{Strumbelj2010,Strumbelj2013}. Specifically, based on what the pot would have been with all possible subsets of the players, each player gets assigned a Shapley value representing their share of the pot. The sum of all Shapley values, which can be negative, is equal to the total pot. When applied to regression models, the pot is analogous to the regression result and the players are analogous to the features. The most common purpose of Shapley values is explaining how different features influence a specific prediction. The absolute values of these Shapley values can, however, also be used as an importance measure. The main drawback of this approach is the fact that calculating Shapley values is computationally expensive.


\subsection{Embedded methods}
With embedded methods, feature selection is an integral part of model construction. A popular example is Lasso regularization for generalized linear models, which uses an $L_1$ penalty for the coefficients associated with the different features. The idea behind this regularization technique is that the $L_1$ norm will drive the coefficients of certain features to 0, effectively eliminating them.






\section{Model training}
\label{s:models}
This section first discusses the different types of models used for PM forecasting, and then goes over sever methods for tuning their hyper-parameters.

\subsection{Types of models}
Once the relevant input features and a specific target value have been chosen, a tabular data set with inputs and corresponding target values can be constructed. With the data in this form, the forecasting problem has been transformed into a regular regression problem. Some commonly used learning models for PM forecasting are Artificial Neural Networks (ANN) \cite{Cecchetti2004,Grivas2005,Hooyberghs2005,Kukkonen2003,McKendry2002,Perez2002}, Support Vector Machines (SVM) \cite{Garcia2018,Georgios2020} and Tree Ensembles such as Random Forests (RF) and XGBoost \cite{Stafoggia2020,Ma2020}.

More recently, also Recurrent Neural Networks including LSTM-based architectures \cite{Abimannam2020,Huang2018,Biancofiore2017} have been proven useful. These approaches are fundamentally different from the ones mentioned above, in the sense that they have an internal state which can capture relevant information from previous inputs.


\subsection{Hyper-parameter tuning and regularization}
All of the learning models mentioned in \cref{s:models} have a number of parameters whose values are not determined via the training process. Hyper-parameter tuning (HPT), which aims at finding the optimal values of these parameters, is a data-dependent problem, and it is a crucial step towards achieving the full potential of the learning models. A prerequisite for every HPT technique is a metric to be used when comparing different hyper-parameter combinations. To prevent overfitting, this metric is usually an estimate of the generalization error, which can be obtained through cross-validation (CV) techniques such as $k$-fold CV. When dealing with time series, however, regular $k$-fold CV might give overly optimistic estimates of the generalization error, because the training data is not guaranteed to be older than the validation data. \Cref{fig:tscv} shows a comparison between regular  $k$-fold CV and an alternative time series split that does guarantee a chronological order between the training and validation data.

\begin{figure}\centering
	\includegraphics[width=\linewidth]{tscv}
	\caption{A comparison of regular and time series split $k$-fold cross-validation.}
	\label{fig:tscv}
\end{figure}

\subsubsection{Grid search and random search}
Besides manual search, where one relies on experience and on knowledge of the inner workings of the learning model to find the best hyper-parameters, there are several automated search methods. The most commonly used one is grid search, where one chooses a set of candidate values for every hyper-parameter, and evaluates all combinations. This method is simple to implement, but can be inefficient. For example, when one of the hyper-parameters does not significantly affect the models' performance, all combinations of all the other hyper-parameters are still evaluated multiple times, once for every candidate value of the low-effect hyper-parameter. An alternative tuning method which addresses this problem is random search, which probes the hyper-parameter space randomly \cite{Bergstra2012}. An illustration of the benefits of random search over grid search is given in \cref{fig:randomsearch}.

\begin{figure}\centering
	\includegraphics[width=\linewidth]{random_search}
	\caption{Illustration of the benefits of random search over grid search for hyper-parameter tuning \cite{Bergstra2012}.}
	\label{fig:randomsearch}
\end{figure}

\subsubsection{Successive Halving Algorithm}
A proposed improvement over random search comes in the form of the Successive Halving Algorithm (SHA) \cite{Jamieson2016}. This algorithm works by repeatedly training all the candidate hyper-parameter configurations with an increasing amount of training data, discarding the worst half of the configurations after each iteration. The free parameter in this algorithm is the amount of initial candidate configurations. An algorithm called Hyperband, proposed by the same authors in a follow-up paper, automates this the choice of this parameter \cite{Jamieson2018}.


\subsubsection{Bayesian hyper-parameter optimization}
Another popular class of hyper-parameter tuning methods are based on Bayesian optimization. These methods iteratively build a model for the conditional probability $p(y|\lambda)$, where $y$ is the model's performance on an evaluation metric, and $\lambda$ is the set of hyper-parameters \cite{Jamieson2018}. At each iteration, the model is used to choose a new configuration to be evaluated, making a trade-off between exploration — choosing configurations with high uncertainty in $y$, and exploitation — choosing configurations with optimal expected $y$. Next, the performance of the new configuration on the metric is used to update $p(y|\lambda)$, after which the process is repeated. Three well-established Bayesian optimization-based hyper-parameter tuning algorithms are Sequential Model-based Algorithm Configuration (SMAC), Tree-structure Parzen Estimator (TPE), and Spearmint. An overview and comparison of  these algorithms is given in \cite{Eggensperger2013}.





\section{Evaluation metrics}
\label{s:eval}
When it comes to evaluating time series forecasts, there are various performance measures. Some commonly used once are shown in \cref{tab:metrics} with their definitions. They each favor different properties, meaning that they might not always agree on the ranking of different forecasts. Therefore, when comparing different results, it is advisable to look at multiple metrics simultaneously.

Some of these measures, like the Mean Absolute Error, the Root Mean Squared Error, and the Mean Absolute Percentage Error are frequently used in regression settings in general, their definitions are self-explanatory. Also the other measures have intuitive explanations. The coefficient of determination $R^2$, for example, is the proportion of the variance in the actual values, which is captured by the forecast. A perfect forecast results in an $R^2$ of $1$. sMAPE is very similar to MAPE, and aims at improving some of its drawbacks \cite{Makridakis1993}. A potential drawback of MAE and RMSE is the fact that they are scale-dependent, which makes it difficult to compare their values over different time series. The two scaled variants, MASE and RMSSE, do not have this problem. MASE (RMSSE) is derived by dividing the MAE (RMSE) of the forecasted values by the MAE (RMSE) of a naive persistence forecast, which always predicts the last observed value \cite{Hyndman2006}. When these scaled errors are lower than one, it means that the forecast outperforms the naive method.

In the context of time series forecasting, a good reference are the Makridakis forecasting competitions or M Competitions.. At the last two editions, M4 and M5, the used performance metrics were sMAPE and MASE, and RMSSE respectively \cite{M4,M5}.

\begin{table}\centering
	\ra{2}
	\begin{tabular}{p{54mm}l>{$\displaystyle}l<{$}>{$}l<{$}}
		\toprule
		Metric& Short&\text{Definition}&\text{Range}\\
		\midrule
		
		Mean Absolute Error &MAE& 
		\frac{1}{h}\sum_{t=1}^{n} \abs{y_t-\hat{y}_t}&
		\left[0,\infty\right[\\
		
		Mean Absolute Scaled Error &MASE&
		\frac{\text{MAE}}{\frac{1}{n-1}\sum_{t=2}^{n}\abs{y_t-y_{t-1}}}&
		\left[0,\infty\right[\\
		
		Root Mean Squared Error &RMSE  &
		 \sqrt{\frac{1}{h}\sum_{t=1}^{n}(y_t-\hat{y}_t)^2}&
		 \left[0,\infty\right[\\
				
		
		Root Mean Squared Scaled Error &RMSSE&
		\frac{\text{RMSE}}{\sqrt{\frac{1}{n-1}\sum_{t=2}^{n}(y_t-y_{t-1})^2}}&
		\left[0,\infty\right[\\
		
		Coefficient of Determination &$R^2$&
		1-\frac{\sum_{t=1}^{n}(y_t-\hat{y}_t)^2}{\sum_{t=1}^{n}(y_t-\bar{y})^2}&
		\left[0,1\right]\\
		
		Mean Absolute \newline Percentage Error&MAPE& \frac{100\%}{h}\sum_{t=1}^{n}\frac{\abs{y_t-\hat{y}_t}}{\abs{y_t}}&
		\left[0,\infty\right[\%\\
		
		Symmetric Mean Absolute \newline Percentage Error&sMAPE& \frac{100\%}{h}\sum_{t=1}^{n}\frac{\abs{y_t-\hat{y}_t}}{\frac{1}{2}(\abs{y_t}+\abs{\hat{y}_t})}&
		\left[0,200\right]\%\\
		
		\bottomrule
	\end{tabular}
	\caption[Definitions of different forecasting performance measures]{Definitions of different forecasting performance measures, with $y_t$ the actual PM concentration at time $t$, $\hat{y}_t$ the forecast and $n$ the length of the test set. The average, $\bar{y}$, used in the definition of $R^2$, is to be taken over the test set.}
	\label{tab:metrics}
\end{table}








