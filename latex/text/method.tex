\chapter{Methodology}
\label{c:method}
This chapter describes the full modeling process, from exploratory data analysis and feature engineering in \cref{s:EDA,s:feateng}, to feature selection in \cref{s:FS}, ending with a description of the different forecasting models and a in \cref{s:reg}


\section{Data exploration}
\label{s:EDA}
Before the modeling can start, it is important to explore the data. \Cref{fig:trend} shows the $\pmtf$ time series for all five considered stations. A clear downwards trend is visible, which would warrant detrending if a strictly autoregressive model were to be  used. In this thesis, however, various external features are used, which should be able to capture the downwards trend. 


In \cref{tab:nan}, an overview of the percentages of missing values for all hourly pollutant time series is given for both the train set (2013-2019) and the test set (2020). To reduce these percentages, single missing hourly values were filled using linear interpolation.




\begin{table}
	\centering
	\begin{tabular}{llrrrrrr}
		\toprule
		&  & \ch{SO2} & \ch{NO2} & \ch{CO} & \ch{O3} &$\pmtf$ & $\pmten$ \\
		station &  &  &  &  &  &  &  \\
		\midrule
		\multirow[t]{2}{*}{BETN043} & train & 6.0 & 3.9 & 11.6 & 5.3 & 10.5 & 11.3 \\
		& test & 4.8 & 3.8 & 10.1 & 4.2 & 9.0 & 8.3 \\
		\multirow[t]{2}{*}{BETN063} & train & 3.1 & 2.6 & 3.4 & 3.8 & 0.3 & 0.3 \\
		& test & 4.1 & 2.9 & 2.8 & 4.8 & 0.5 & 0.5 \\
		\multirow[t]{2}{*}{BETR222} & train & 3.1 & 3.0 & 5.1 & 4.6 & 0.8 & 0.8 \\
		& test & 5.8 & 3.8 & 6.0 & 5.0 & 1.0 & 1.0 \\
		\multirow[t]{2}{*}{BETR001} & train & 12.9 & 3.6 & 9.0 & 2.3 & 7.2 & 7.3 \\
		& test & 4.3 & 3.6 & 10.6 & 3.7 & 7.9 & 7.9 \\
		\multirow[t]{2}{*}{BETR802} & train & 100.0 & 4.2 & 6.2 & 100.0 & 1.4 & 1.4 \\
		& test & 100.0 & 4.7 & 5.8 & 100.0 & 2.9 & 4.4 \\
		\bottomrule
	\end{tabular}
	\caption{Percentage of missing values in the pollutant hourly time series.}
	\label{tab:nan}
\end{table}

\begin{figure}\centering
	\includegraphics[width=\linewidth]{trends}
	\caption{$\pmtf$ time series for the five considered measuring stations.}
	\label{fig:trend}
\end{figure}




\section{Forecasting setup and candidate features}
\label{s:feateng}
The target of the forecast is be the average $\pmtf$ concentration of the next day. To make the results useful for practical applications, only observations up until 00:18 h of the present day will be considered as inputs. The three types of candidate features are: concentration measurements, meteorological variables and temporal variables. All forecasting models will be trained for the five considered stations separately, so no geographical features will be used. In the following, all considered features will be described type by type. An overview of all candidate features is given in \cref{tab:features}.

\begin{table}\centering
	\begin{tabularx}{\textwidth}{ll>{\RaggedRight\arraybackslash}X}
		\toprule
		Type & Feature & Explanation\\
		\midrule
		Pollutant& \texttt{X\_00}& 1-h average concentration of pollutant X at 00:00 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		& \texttt{X\_06}& 1-h average concentration of pollutant X at 00:06 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		& \texttt{X\_12}& 1-h average concentration of pollutant X at 00:12 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		& \texttt{X\_18}& 1-h average concentration of pollutant X at 00:18 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		& \texttt{X\_AVG18}& 24-hour average concentration of pollutant X at 00:18 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		& \texttt{X\_MAX18}& 24-hour maximum concentration of pollutant X at 00:18 h of the present day, included for \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and $\pmtf$.\\
		
		Meteorological&\texttt{var\_AVG\_F} & Forecasted average for variable \texttt{var} on the next day, included for all 23 meteorological variables discussed in \cref{ss:meteovars}.\\
		& \texttt{t2m\_MAX\_F} & Forecasted maximum temperature for the next day.\\
		& \texttt{t2m\_DIFF\_F} & Forecasted temperature range for the next day.\\
		& \texttt{t2m\_MAX18} & Maximum temperature between 00:18 h of the previous day and 00:18 h of the present day\\
		& \texttt{t2m\_DIFF18} & Temperature range between 00:18 h of the previous day and 00:18 h of the present day\\
		& \texttt{wsp\_AVG18} & Average wind speed between 00:18 h of the previous day and 00:18 h of the present day\\
		
		Temporal&\texttt{yearly\_sin}& Sine of the day of the year\\
		&\texttt{yearly\_cos}& Cosine of the day of the year\\
		&\texttt{weekly\_sin}& Sine of the day of the week\\
		&\texttt{weekly\_cos}& Cosine of the day of the week\\
		&\texttt{DoW}& Day of the week (0-6)\\
		&\texttt{weekday}& Boolean indicator: 1 on weekdays, 0 on workdays\\
		
		\bottomrule
	\end{tabularx}
	\caption{Overview of all candidate features.}
	\label{tab:features}
\end{table}


\subsection{Pollutant features}
The considered pollutants are \ch{NO2}, \ch{SO2}, \ch{CO}, \ch{O3}, $\pmten$ and of course $\pmtf$. These are available for all five stations, except for BETR802 for which there are no \ch{SO2} and \ch{O3} measurements. From each of these pollutants, 6 different features are created: four hourly values at 00:00 h, 00:06 h, 00:12 h and at 00:18 h of the present day and the 24-hour average and maximum concentration at 00:18 h of the present day. For the 24-hour aggregate features, up to four missing values were tolerated. In total, there are 36 (24 for BETR802) candidate pollutant features. 


\subsection{Meteorological features}
\label{ss:meteovars}
The basis for the meteorological features are the 20 ERA5 variables listed in \cref{tab:era5vars}. Most of these are used as they are, but some are transformed into more sensible forms. The $u$ and $v$ components of the wind (\texttt{u10} and \texttt{v10}), for example, are transformed into the following three new variables:

\begin{align}
	\text{Wind speed: }&& \texttt{wsp} &= \sqrt{u10^2+v10^2},\\ 
	\text{Wind direction North-South: }&& \texttt{wdir\_sin} &= \frac{\texttt{v10}}{\texttt{wsp}},\\ 
	\text{Wind direction West-East: }&& \texttt{wdir\_cos} &= \frac{\texttt{u10}}{\texttt{wsp}}.
\end{align}
The reason why the wind direction variable is split into a North-South component (sine) and a West-East component (cosine), is the fact that a regular wind direction variable has a discontinuity at 360°.

Another transformed variable is the relative humidity, which is not archived directly in the ERA5 data set. It can be derived from the pressure $p$, temperature ($T$) and dew point temperature ($T_{dp}$) as follows.

\begin{equation}
	\text{RH} = 100\%\frac{q_{sat}(T_{dp})}{q_{sat}(T)}
\end{equation}
with 
\begin{align}
	q_{sat}(T) &= \cfrac{\frac{R_{dry}}{R_{vap}}e_{sat}(T)}{p-\left(1-\frac{R_{dry}}{R_{vap}}e_{sat}(T)\right)},\\
	e_{sat}(T)&=a_1\exp \left( a_3\frac{T-T_0}{T-a4}\right),
\end{align}
where $a1= \SI{611.21}{\pascal}$, $a_3=17.502$, $a4 = \SI{32.19}{\kelvin}$, $\frac{R_{dry}}{R_{vap}}=0.621981$ \cite[Chapter~7]{IFC_doc}.  To reduce the risk of excluding useful variables, both relative humidity and dew point temperature will be kept. 

The transformation of the wind variables, and the addition of the relative humidity puts the total number of meteorological variables at 23. A first set of features derived from these variables are the forecasted averages for the next day. Additionally, also the forecasted maximum temperature (\texttt{t2m\_MAX\_F}), and the forecasted 24-hour temperature range (\texttt{t2m\_DIFF\_F}: $T_{max}-T_{min}$) are included. And lastly, the 24 h average wind speed at 00:18 h of the present day (\texttt{wsp\_AVG18}) and the maximum temperature and the temperature range between 00:18 h of the previous day and 00:18 h of the present day are added (\texttt{t2m\_MAX18} and \texttt{t2m\_DIFF18}), analogous to \cite{Perez2002}. The total number of candidate meteorological features is 28.


\subsection{Temporal features}
To capture the the time-varying nature of $\pmtf$,  7 temporal features are considered. Firstly, to capture the weekly and yearly cycles, a sine and a cosine variable of the day of the week and the day of the year are added. The specific definitions of these variables are given by

\begin{align}
	\text{\texttt{yearly\_sin}} &= \sin\left(\frac{\text{day of the year}}{365}2\pi\right),\\
	\text{\texttt{yearly\_cos}} &= \cos\left(\frac{\text{day of the year}}{365}2\pi\right),\\
	\text{\texttt{weekly\_sin}} &= \sin\left(\frac{\text{day of the week}}{7}2\pi\right),\\
	\text{\texttt{weekly\_cos}} &= \cos\left(\frac{\text{day of the week}}{7}2\pi\right).
\end{align}

Finally, also the day of the week (\texttt{DoW}) and a boolean variable indicating whether the day is a weekday of a weekend day (\texttt{weekday}) are included.




\section{Feature selection}
\label{s:FS}
In total, there are 70 candidate features, and it is likely that a substantial part of those are redundant or irrelevant. This section will describe the process of eliminating those features.

The main part of the feature selection will be done using the Boruta algorithm, with Shap values as importance measure. However, since this algorithm is computationally expensive, the pool of candidate features will first be reduced using two filter methods. Since these filter steps are not very sophisticated, applying them too strictly would entail a high risk of eliminating useful features. To minimize this risk, each filter step will be performed for each of the five station separately, and only features that fail for all stations will be eliminated.

\subsection{Correlation filter}
Some of the candidate features are highly correlated. For example, the following pairs of features have a pearson correlation coefficient higher than 0.95 for station BETN043:

\begin{itemize}\itemsep0em
	\item \texttt{ssrd\_AVG\_F} and \texttt{ssr\_AVG\_F}
	\item \texttt{slhf\_AVG\_F} and \texttt{e\_AVG\_F}.
\end{itemize}
For each of such pairs, one variable has to be chosen to fail the filter test. The choices of elimination are based on Occam's razor: features with simpler definitions are preferred. In this case, the evaporation feature \texttt{e\_AVG\_F} and the surface solar radiation feature \texttt{ssr\_AVG\_F} are retained in favor of  \texttt{ssdr\_AVG\_F} (surface solar radiation downwards) and \texttt{slhf\_AVG\_F} (surface latent heat flux). Those two failed features were the only ones that failed the filter for every station.


\subsection{Noise filter}
In this step, noisy features are identified by analyzing how consistent the relation between each feature and the regression target is. This was done as follows. First, the training data is split into two parts, one with the years 2013 to 2018, and one with only the year 2019. Next, each feature is binned, and for both parts of the data the average target value is calculated for each bin. The idea is that for noisy features the bin averages for the first data set will have a low correlation with the bin averages for the second data set. All features for which this correlation is lower than 85\% for all of the stations fail the test. An illustration for a feature that passes the test is given in \cref{fig:noise}. For the implementation of this filter, the Python package \texttt{featexp} was used \cite{featexp}.
\begin{figure*}
	\centering
	\begin{subfigure}[b]{0.475\textwidth}
		\centering
		\includegraphics[width=\textwidth]{blh_train}
		\caption{Bin averages for 2013-2018.}    
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.475\textwidth}  
		\centering 
		\includegraphics[width=\textwidth]{blh_val}
		\caption{Bin averages for 2019.}    
	\end{subfigure}
	\caption[Illustration of the noisy feature detection method]{Illustration of the noisy feature filter method for the boundary layer height feature (\texttt{blh\_AVG\_F}) In this case, the correlation is high (0.95), and the feature is kept.}
	\label{fig:noise}
\end{figure*}

The features that did not make it past the two filters are

\begin{itemize}
	\item Evaporation from bare soil (\texttt{evabs\_AVG\_F}),
	\item High cloud cover (\texttt{hcc\_AVG\_F}),
	\item Runoff (\texttt{ro\_AVG\_F}),
	\item Surface solar radiation (\texttt{ssr\_AVG\_F}),
	\item Surface solar radiation downwards (\texttt{ssrd\_AVG\_F}),
	\item Surface latent heat flux (\texttt{slhf\_AVG\_F})
	\item and Surface thermal radiation (\texttt{str\_AVG\_F}).
\end{itemize}


\subsection{Boruta}
From the final 63 features, the relevant ones are identified on a per station basis using the Boruta. The algorithm is run twice, once using the mean absolute Shapley values as importance measure, and once using the mean decrease in RSS (Residual Sum of Squared). These two variants of the algorithm will from here on be referred to as Boruta-Shapley and Boruta-RSS. For the implementation, the Python package Boruta-Shap, which supports both importance measures, was used \cite{borutashap}. For each station, the algorithm was run for 100 iterations or until all features were either accepted or rejected. The results for both importance measures will be discussed in the next \namecref{c:results}.



\section{Regression}
\label{s:reg}
In this section, five different regression models will be tuned and trained. A description of the hyper-parameters that will be tuned for each of the models is shown in \cref{tab:hpdistr}.

\subsection{Model details}
The first model is regular linear regression using Lasso regularization for the coefficients. The parameter $\alpha$ determines the relative weight of the regularization term.
Next, two Support Vector Machines are used, one with a polynomial kernel and one with a radial basis function (RBF) kernel. The two kernels are defined as

\begin{align*}
	\kappa_{\text{poly}}(x,x^\prime) &= (\gamma\langle x,x^\prime\rangle+c_0)^d,\\
	\kappa_{\text{rbf}}(x,x^\prime) &= \exp(-\gamma\lVert x-x^\prime\rVert^2).
\end{align*}
In addition to the kernel parameters $\gamma$, $c_0$ and $d$, SVM regression models have two more hyper-parameters.  These two hyper-parameters, $C$ and $\epsilon$ can be seen in the primal form of the SVM regression optimization problem:

\begin{subequations}
	\label{eq:mpc}
	\begin{alignat}{2}
		\boldminim \quad &\frac{1}{2}\lVert\bm{w}\rVert^2 + C\sum_{i=1}^{N}(\xi_i-\xi_i^\ast)&&\nonumber\\
		\st \quad & y_i - \bm{w}\cdot\varphi(\bm{x_i})-b\leq \epsilon +\xi_i&&\nonumber\\
							&\bm{w}\cdot\varphi(\bm{x_i}) + b - y_i\leq \epsilon+ \xi_i^\ast&&\nonumber\\
							&\xi_i,\xi_i^\ast\geq 0,i=1,\dots N.
	\end{alignat}
\end{subequations}
The parameter $C$ is inversely proportional to the strength of the $L_2$ regularization. The parameter $\epsilon$ is the margin used in the $\epsilon$-insensitive hinge-loss.

The final two models are two decision tree ensembles, namely Random Forest and XGBoost. Both methods combine multiple decision trees, but the former uses bagging while the latter uses boosting. Specifically, with random forests, the model output is the average prediction over a collection or bag of trees trained on different subsets of the data. The approach of XGBoost is to iteratively boost the performance by adding new trees aimed at correcting the mistakes of the previous ensemble \cite{XGBoost}. 

\subsection{Hyper-parameter tuning}
For each model, the hyper-parameters will be tuned using random search, sampling 50 candidate hyper-parameter configurations from the distributions shown in \cref{tab:hpdistr}. A comparison with different hyper-parameter tuning algorithms will be made in the next chapter. The different hyper-parameter configurations are evaluated on the average RMSE over a 4-fold time series split of the training data. The four splits consist of the following years of data for training and validation:

\begin{enumerate}
	\item Training: 2013-2015 | Validation: 2016,
	\item Training: 2013-2016 | Validation: 2017,
	\item Training: 2013-2017 | Validation: 2018,
	\item Training: 2013-2018 | Validation: 2019.
\end{enumerate}

Each of the models is be tuned and trained for each of the stations and feature sets separately. Although not all models require this, from now on, all input features will be shifted to zero mean and scaled to unit variance. The same transformations will be applied to the test data when evaluating the models. The performances of the tuned models on the test sets will be discussed in the next \namecref{c:results}.

\begin{table}\centering
	\begin{tabular}{llp{55mm}p{35mm}}
		\toprule
		Model&Param.& Description& Sample distribution\\
		\midrule
		Lasso			& $\alpha$&$L_1$ regularization factor&$\log_{10}(\alpha)\sim \mathcal{U}(-3, 1)$\\
		SVM-rbf		&$C$&$L_2$ regularization factor &$\log_{10}(C)\sim \mathcal{U}(-1, 3)$\\
							&$\epsilon$ &Margin for $\epsilon$-insensitive hinge-loss &$\log_{10}(\epsilon)\sim \mathcal{U}(-3, 0)$\\
							&$\gamma$ &Scale of the rbf-kernel &$\log_{10}(\gamma)\sim \mathcal{U}(-5, 0)$\\
		SVM-poly &$C$&$L_2$ regularization factor& $\log_{10}(C)\sim \mathcal{U}(-1, 3)$\\
							&$\epsilon$ &Margin for $\epsilon$-insensitive hinge-loss &$\log_{10}(\epsilon)\sim \mathcal{U}(-3, 0)$\\
							& $d$&Degree of the polynomial kernel& $d\sim  \mathcal{U}\{1,6\}$\\
							&$\gamma$ &Polynomial kernel coefficient &$\log_{10}(\gamma)\sim \mathcal{U}(-5, 0)$\\
							&$c_0$ &Independent term in the polynomial kernel &$\log_{10}(c_0)\sim \mathcal{U}(-5, 0)$\\
		RF				&$n_{est}$ &Number of estimators&$n_{est}\sim  \mathcal{U}\{50,5000\}$\\
							&$d_{max}$& Maximum tree depth&$d_max\sim \mathcal{U}\{1,10\}$\\
							&$f_{max}$& Number of considered features when looking for best split&All features or square root of all features\\
							&$ms_{split}$& Minimum number of samples required to split an internal node& $ms_{split}\sim \mathcal{U}\{2,11\}$\\
							&$ms_{leaf}$& Minimum number of samples required to be a leaf node& $ms_{leaf}\sim \mathcal{U}\{1,11\}$\\
							&B& Whether bootstrap samples are used when building trees& $B\sim \text{Bernouli}(\frac{1}{2})$\\
		XGBoost		&$n_{est}$ &Number of estimators&$n_{est}\sim  \mathcal{U}\{50,5000\}$\\
							&$\eta$& Learning rate& $\log_{10}(\eta)\sim \mathcal{U}(-2, 0)$\\
							&$d_{max}$& Maximum tree depth&$d_max\sim \mathcal{U}\{1,10\}$\\
							& $s$& Subsample ratio of the training instances& $s\sim \mathcal{U}(0.1,0.9)$\\
							&$sc_{tree}$& Subsample ratio of columns when constructing each tree&$sc_{tree}\sim \mathcal{U}(0.1,0.9)$\\
							&$\alpha$& $L_1$ regularization factor&$\log_{10}(\alpha)\sim \mathcal{U}(-9, 2)$\\
							&$\lambda$& $L_2$ regularization factor&$\log_{10}(\lambda)\sim \mathcal{U}(-9, 2)$\\
							
		\bottomrule
	\end{tabular}
	\caption{Hyper-parameters for the different models and the chosen sample distributions for the random search.}
	\label{tab:hpdistr}
\end{table}





