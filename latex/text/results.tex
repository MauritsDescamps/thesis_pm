\chapter{Results}
\label{c:results}

\section{Feature selection results}
The features selected for each station by the Boruta algorithm using the Shapley and RSS importance measures are shown in \cref{tab:borutashapres,tab:borutarssres} respectively. Although these algorithms were originally only given the features that passed the two filter steps discussed in \cref{s:FS} to save time, they were rerun using the full set of candidate features. None of the features originally filtered out were selected by the Boruta algorithm, demonstrating the usefulness of the filter steps.

Based on these results, some observations can be made. Firstly, more features are selected by Boruta-Shapley than by Boruta-RSS. The features selected for all stations by Boruta-RSS are, however, a subset of those selected by Boruta-Shapley. This indicates that both algorithms might agree, were the feature selection threshold of Boruta-RSS to be lowered. In the remainder of this section, the selected meteorological, pollutant and temporal features will be discussed separately. The focus of the discussion will be on the features selected by Boruta-Shapley.

\subsection{Meteorological features}
The selected features themselves are mostly in agreement with literature. Among the consistently selected forecasted meteorological variables, wind speed, wind direction, boundary layer height, precipitation and average temperature are all frequently used features in literature. With regards to variables relating to humidity, two features have been used in literature, namely the relative humidity and the dew point temperature. Both were considered as candidate inputs in this experiment, but only dew point temperature was selected, indicating that it is better suited for PM forecasting.
A more surprising selected meteorological feature is the forecasted Surface Sensible Heat Flux (SSHF), which is \textit{the transfer of heat between the Earth's surface and the atmosphere through the effects of turbulent air motion (but excluding any heat transfer resulting from condensation or evaporation)}\cite{era5}. The relation between this variable and PM concentrations might be similar to the relation between PM and the boundary layer height, because both BLH and SSHF are closely related to air turbulence. 
Besides the forecasted average temperature, which is selected for all stations, the forecasted temperature difference is selected for four out of five stations, indicating that it is closely related to PM concentrations.
A last observation with respect to the meteorological features is the fact that the candidate meteorological features from the present day, i.e. the temperature range (\texttt{t2m\_DIFF18}), the maximum temperature (\texttt{t2m\_MAX18}) and the average wind speed (\texttt{wsp\_AVG18}), are also occasionally selected, albeit for only one, one and two stations respectively.

\subsection{Pollutant features}
When it comes to the selected pollutant features, there are also few surprises. $\pmtf$ concentrations, both lagged and aggregated, are most often selected, indicating that there is a considerable potential for autoregressive forecasting. Additionally, also $\pmten$-related features are often selected, which was to be expected since $\pmtf$ and $\pmten$ are closely related.
Besides PM-related features, also other pollutants turn out te be important. The main exogenous pollutant is \ch{NO2}, with both lagged and aggregated features selected for all stations but BETN043, which is an industrial - suburban station. Other occasionally selected pollutant features are \texttt{CO\_0} (1 station), \texttt{CO\_AVG18} (1 station) and \texttt{O3\_18} (2 stations).
A pollutant which is surprisingly absent from the selections it \ch{SO2}. Even though this pollutant has been identified as relevant for PM forecasting in the past \cite{Cecchetti2004,Rumaling2022,Dutta2021}, it doesn't seem to be important in Belgium.

\subsection{Temporal features}
The only often selected temporal feature is the sine of the day of the year (\texttt{yearly\_sin}).

\begin{table}\centering
	\ra{1.5}
	{\RaggedRight
		\begin{tabular}{p{24mm}p{4cm}p{42mm}p{18mm}}
			\toprule
			&\multicolumn{3}{c}{Feature type}\\
			\cline{2-4}
			&Pollutant & Meteorological & Temporal\\
			\midrule
			Accepted for every station:
			&\texttt{PM2.5\_12}, \texttt{PM2.5\_18}, \texttt{PM2.5\_AVG18}, \texttt{PM10\_18}
			&\texttt{d2m\_AVG\_F}, \texttt{sshf\_AVG\_F},  \texttt{wsp\_AVG\_F}, \texttt{wdir\_cos\_AVG\_F}, \texttt{blh\_AVG\_F}, \texttt{tp\_AVG\_F}, \texttt{t2m\_AVG\_F}, \texttt{mcc\_AVG\_F}
			&\texttt{yearly\_sin}\\
			
			Additionally accepted for&&&\\
			BETN043
			&\texttt{PM2.5\_0}, \texttt{PM2.5\_MAX18}, \texttt{PM10\_12}, \texttt{O3\_18}
			& \texttt{t2m\_DIFF\_F}, \texttt{t2m\_MAX18}, \texttt{t2m\_DIFF18},  \texttt{wdir\_sin\_AVG\_F},
			&\\
			
			BETN063
			&\texttt{PM2.5\_0}, \texttt{PM2.5\_MAX18}, \texttt{PM10\_AVG18}, \texttt{PM10\_MAX18}
			\texttt{NO2\_0}, \texttt{NO2\_6}, \texttt{NO2\_12}, \texttt{NO2\_18}, \texttt{NO2\_AVG18}, \texttt{CO\_AVG18}
			&\texttt{t2m\_DIFF\_F}, \texttt{strd\_AVG\_F}, \texttt{wsp\_AVG18}, \texttt{wdir\_sin\_AVG\_F}, \texttt{sp\_AVG\_F}
			&\\
			
			
			BETR001
			&\texttt{PM2.5\_6}, \texttt{PM2.5\_MAX18},
			\texttt{NO2\_12}, \texttt{NO2\_18},  \texttt{NO2\_AVG18}, \texttt{NO2\_MAX18},
			\texttt{CO\_0}
			& \texttt{t2m\_DIFF\_F}, \texttt{wsp\_AVG18}, \texttt{wdir\_sin\_AVG\_F}, \texttt{strd\_AVG\_F}
			&\\
			
			
			BETR222
			&\texttt{PM2.5\_0}, \texttt{PM10\_AVG18}, \texttt{NO2\_12}, \texttt{NO2\_18}, \texttt{NO2\_AVG18}, \texttt{O3\_18}
			&\texttt{t2m\_MAX18}, \texttt{wdir\_sin\_AVG\_F}, \texttt{lcc\_AVG\_F}, \texttt{strd\_AVG\_F}
			&\\
			
			
			
			BETR802
			&\texttt{PM2.5\_MAX18}, \texttt{PM10\_AVG18}, \texttt{NO2\_12}, 	\texttt{NO2\_18},  \texttt{NO2\_AVG18}
			&\texttt{t2m\_DIFF\_F}, \texttt{t2m\_MAX18}, \texttt{strd\_AVG\_F}, \texttt{sp\_AVG\_F}, 
			&\\
			
			
			\bottomrule
		\end{tabular}
	}
	\caption[Boruta-Shap results]{Features selected by the Boruta algorithm for the different stations, using Shapley values as importance measure.}
	\label{tab:borutashapres}
\end{table}

\begin{table}\centering
	\ra{1.5}
	{\RaggedRight
		\begin{tabular}{p{24mm}p{4cm}p{42mm}p{18mm}}
			\toprule
			&\multicolumn{3}{c}{Feature type}\\
			\cline{2-4}
			&Pollutant & Meteorological & Temporal\\
			\midrule
			Accepted for every station:
			&\texttt{PM2.5\_18}, \texttt{PM2.5\_AVG18}, \texttt{PM10\_18}
			&\texttt{wsp\_AVG\_F}, \texttt{wdir\_cos\_AVG\_F},  \texttt{blh\_AVG\_F}, \texttt{sshf\_AVG\_F}
			&\\
			
			
			Additionally accepted for&&&\\
			BETN043
			&\texttt{PM2.5\_12}, \texttt{PM2.5\_MAX18}, \texttt{O3\_MAX18}
			&\texttt{t2m\_DIFF\_F}, \texttt{d2m\_AVG\_F}
			&\texttt{yearly\_sin}\\
			
			BETN063
			&\texttt{PM2.5\_12}, \texttt{PM10\_AVG18}, \texttt{NO2\_6}, \texttt{NO2\_18}, \texttt{NO2\_AVG18}
			&\texttt{t2m\_AVG\_F}, \texttt{wdir\_sin\_AVG\_F}, \texttt{d2m\_AVG\_F}, \texttt{strd\_AVG\_F},
			&\texttt{yearly\_sin}\\
			
			
			
			BETR001
			&\texttt{PM2.5\_12}, \texttt{PM2.5\_MAX18}, \texttt{PM10\_0}, \texttt{NO2\_6}, \texttt{NO2\_18}, \texttt{NO2\_AVG18}
			&\texttt{t2m\_AVG\_F},  \texttt{wdir\_sin\_AVG\_F}, \texttt{strd\_AVG\_F}
			&\\

			
			BETR222
			&\texttt{PM10\_AVG18}, \texttt{NO2\_18}, \texttt{NO2\_AVG18},
			&\texttt{t2m\_AVG\_F}, \texttt{wdir\_sin\_AVG\_F}, \texttt{d2m\_AVG\_F}, \texttt{strd\_AVG\_F}
			&\texttt{yearly\_sin}\\
			
			
			BETR802
			&\texttt{PM2.5\_12}
			&\texttt{t2m\_AVG\_F}, \texttt{tp\_AVG\_F},
			&\texttt{yearly\_sin}\\
			
			
			\bottomrule
		\end{tabular}
	}
	\caption[Boruta-Shap results]{Features selected by the Boruta algorithm for the different stations, using reduction in RSS as importance measure.}
	\label{tab:borutarssres}
\end{table}


\section{Hyper-parameter optimization comparison}
In \cref{tab:HPSVM}, four different hyper-parameter tuning methods, namely grid search, random search, Bayesian optimization and Successive Halving (SHA), were compared on the SVM model with RBF kernel and on the XGBoost model. For the grid search, random search and SHA methods, the implementations from the Python package scikit-learn were used \cite{sklearn_api}. For the Bayesian method, the \texttt{BayesSearchCV} function from the scikit-optimize Python package was used with default configurations \cite{scikit-opt}. For the random search, 50 configurations were sampled from the hyper-parameter distributions given in \cref{tab:hpdistr}. Those distributions were also used to sample the initial candidates for the SHA algorithm.

For the SVM model, which has only three tunable parameters, the SHA and random methods reach the lowest cross-validation RMSE, but SHA is more than twice as fast. Grid search, for which a grid with 72 parameter configurations was used, reaches comparable performance but takes up considerably more time. The most inefficient, and also the worst performing method was Bayesian optimization in this case.

The results for XGBoost, for which seven parameters were tuned, tell a different story. Most noticeable is the average grid search timing of a little under 7 hours, which is a result of the curse of dimensionality. Specifically, for each of the 7 hyper-parameters, three different values were considered, resulting in a grid of 2187 configurations. The best performing method, both in terms of cross-validation RMSE and timing, was random search, followed closely by Bayesian optimization. SHA is still the fastest method, but has the worst performance here.

Based on these results, a few conclusions can be made. The overall preferred method is random search, because it achieves the best results with both small and larger hyper-parameter spaces. Specifically for low-dimensional search spaces, SHA is good choice, especially when a low computational cost is highly valued. For high-dimensional spaces, the benefits of Bayesian optimization become clear.



\begin{table}
	\centering
	\ra{1}
	\begin{tabular}{lllll}
		\toprule
		& \multicolumn{2}{l}{SVM\_rbf} & \multicolumn{2}{l}{XGBoost} \\

		& RMSE & time [s] & RMSE & time [s] \\
		\midrule
		SHA & 5.22 & 16 & 5.38 & 669 \\
		Bayes & 5.48 & 132 & 5.26 & 966 \\
		Grid & 5.23 & 114 & 5.29 & 24,347 \\
		Random & 5.22 & 35 & 5.25 & 779 \\
		\bottomrule
	\end{tabular}
	\caption{Average optimal cross-validation RMSE and search time for different hyper-parameter optimization methods.}
	\label{tab:HPSVM}
\end{table}

\section{Test results}
\Cref{tab:testres} gives the test set results on the metrics MASE, RMSE and SMAPE of the different forecasting models discussed in \cref{c:method}, using the features selected by the Boruta-Shapley and the Boruta-RSS algorithms. All models were tuned using random search, and all results are averaged over the 5 stations. For comparison, also the performances of a naive persistence model are included. This model always predicts the next day's average concentration to be the same as that on the present day.

On all metrics, the SVM model using the RBF kernel performs best. A shared second place goes to SVM with polynomial kernel and XGBoost, which have similar performances on all metrics. Next model, performance-wise, is Random Forest. Finally, the baseline Lasso model only slightly outperforms the naive persistence model.

Furthermore, all models except Random Forest perform better on the Shapley feature set than on the RSS feature set. This suggests that Shapley values are a better indication of feature importance than the decrease in the residual sum of squares (RSS. There is, however, a trade-off to be made, since an iteration of the Boruta algorithm using the Shapley importance measure took seconds around 200 seconds on average, while an iteration using RSS only took 2 seconds.


\begin{table}\centering
	\begin{tabular}{llrrr}
		\toprule
		&  & MASE & RMSE & SMAPE \\
		\midrule
		& Naive & 1.00 & 5.83 & 48.59 \\
		Shapley& Lasso & 0.94 & 4.70 & 49.17 \\
		& SVM\_rbf & $\bm{0.75}$ & $\bm{4.14}$ & $\bm{37.72}$ \\
		& SVM\_poly & 0.80 & 4.33 & 41.81 \\
		& XGBoost & 0.80 & 4.34 & 39.69 \\
		& Random Forest & 0.88 & 4.56 & 42.66 \\
		RSS& Lasso & 1.01 & 4.95 & 51.93 \\
		& SVM\_rbf & 0.79 & 4.32 & 38.73 \\
		& SVM\_poly & 0.83 & 4.49 & 41.54 \\
		& XGBoost & 0.84 & 4.50 & 40.67 \\
		& Random Forest & 0.86 & 4.51 & 42.48 \\
		\bottomrule
	\end{tabular}
	\caption[Test set results using the Boruta-Shapley and the Boruta-RSS features.]{Test set results using the Boruta-Shapley and the Boruta-RSS features. The results of the naive persistence model are given for comparison.}
	\label{tab:testres}
\end{table}